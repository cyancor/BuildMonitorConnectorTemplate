Connector =
{
    // The form to configure the connector.
    FormData:
    {
        Name: { Type: "text", Title: "Name", Value: "Connector Template" },
        JsonFile: { Type: "text", Title: "JSON File", Value: "https://run.buildmonitor.io/status.json", Required: true, Restart: true, ProtocolCheck: true },
        Interval: { Type: "number", Title: "Request Interval (in milliseconds)", Value: 10000, Required: true }
    },

    Index: 0,

    Shared:
    {
        StateMap:
        {
            "OK": "Success",
            "Error": "Failed"
        }
    },

    _refreshTimer: null,
    _refreshing: false,

    _projects: null,

    Logger: null,
    Requestor: null,
    DataSink: null,
    ConnectorConfig: null,

    Initialize: function()
    {
        this._projects = {};
    },

    Start: function()
    {
        if (this._refreshTimer != null)
        {
            this.Logger.Log("Already started.", this.Logger.LogLevels.Warning, this);
            return;
        }

        this._refreshTimer = setInterval(this.Refresh.bind(this), this.ConnectorConfig.Interval);
        this.Refresh();
    },

    Stop: function()
    {
        if (this._refreshTimer == null)
        {
            return;
        }

        clearInterval(this._refreshTimer);
        this._refreshTimer = null;
    },

    Refresh: function()
    {
        if (this._refreshing)
        {
            this.Logger.Log("Already refreshing, skipping...", this.Logger.LogLevels.Info, this);
            return;
        }

        this.FetchJson(this.ConnectorConfig.JsonFile);
    },

    FetchJson: function(url)
    {
        this._refreshing = true;

        this.Logger.Log("Fetching " + url + "...", this.Logger.LogLevels.Debug, this);

        var options = {
            Headers: { },
            DataType: "json",
            MimeType: "application/json"
        };

        var self = this;
        this.Requestor.Get(url,
            function(response) { self.JsonFetched.call(self, url, response); },
            function(jqXhr, textStatus, errorThrown) { self.FetchFailed.call(self, url, jqXhr, textStatus, errorThrown); },
            function()
            {
                // This delegate gets always called after successful or erroneous response
                self._refreshing = false;
            },
            options);
    },

    FetchFailed: function(url, jqXhr, textStatus, errorThrown)
    {
        this.Logger.Log("Request for " + url + " failed: " + errorThrown + " (" + textStatus + ")", this.Logger.LogLevels.Warning, this);

        var identifier = this.DataSink.GetValidIdentifier(url);
        this.UpdateProject(identifier, url, "Failed", errorThrown + " (" + textStatus + ")");
    },

    JsonFetched: function(url, json)
    {
        var identifier = this.DataSink.GetValidIdentifier(url);
        this.UpdateProject(identifier, url, this.Shared.StateMap[json.Status] || "Unknown", "");
    },

    UpdateProject: function(identifier, name, status, message)
    {
        this._projects[identifier] = this._projects[identifier] || { };
        var project = this._projects[identifier];

        project.Identifier = identifier;
        project.Name = name;
        project.Status = status;
        project.Reference = message;

        this.DataSink.Update(this._projects, null, this);
    }
};